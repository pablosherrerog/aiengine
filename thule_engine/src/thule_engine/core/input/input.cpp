#include "input.hpp"

#include <GLFW/glfw3.h>

#include "thule_engine/core/application.hpp"

bool Input::isKeyPressed(const KeyCode key)
{
    auto *window = static_cast<GLFWwindow *>(Application::getApp().getWindow().getNativeWindow());
    auto state = glfwGetKey(window, static_cast<int32_t>(key));
    return state == GLFW_PRESS || state == GLFW_REPEAT;
}

bool Input::isKeyReleased(const KeyCode key) { return !Input::isKeyPressed(key); }

bool Input::isMouseButtonPressed(const MouseCode button)
{
    auto *window = static_cast<GLFWwindow *>(Application::getApp().getWindow().getNativeWindow());
    auto state = glfwGetMouseButton(window, static_cast<int32_t>(button));
    return state == GLFW_PRESS;
}

glm::vec2 Input::getMousePosition()
{
    auto *window = static_cast<GLFWwindow *>(Application::getApp().getWindow().getNativeWindow());
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);

    return {(float)xpos, (float)ypos};
}

float Input::getMouseX() { return getMousePosition().x; }

float Input::getMouseY() { return getMousePosition().y; }
