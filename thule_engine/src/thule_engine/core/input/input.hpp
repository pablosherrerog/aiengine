#pragma once

#include <glm/glm.hpp>

#include "thule_engine/core/input/keyCodes.hpp"
#include "thule_engine/core/input/mouseCodes.hpp"

class Input
{
   public:
    static bool isKeyPressed(KeyCode key);
    static bool isKeyReleased(KeyCode key);
    static bool isMouseButtonPressed(MouseCode button);
    static glm::vec2 getMousePosition();
    static float getMouseX();
    static float getMouseY();
};
