#include "thule_engine/core/commonCoreUtils.hpp"

#include <map>
#include <string>
#include <typeindex>

#include "thule_engine/components/simpleAnimation.hpp"

// https://stackoverflow.com/questions/37641427/c-map-of-string-as-key-and-type-as-value
// CommonCoreUtils::OpMap2 CommonCoreUtils::componentNames = CommonCoreUtils::init_map();
using TypeStrMap = std::map<std::type_index, std::string>;
TypeStrMap CommonCoreUtils::componentNames = {{typeid(Metadata), "Metadata"},
                                              {typeid(Relationship), "Relationship"},
                                              {typeid(Transform), "Transform"},
                                              {typeid(DirectionalLight), "DirectionalLight"},
                                              {typeid(Geometry), "Geometry"},
                                              {typeid(Material), "Material"},
                                              {typeid(PointLight), "PointLight"},
                                              {typeid(Renderable), "Renderable"},
                                              {typeid(SpotLight), "SpotLight"},
                                              {typeid(Camera), "Camera"},
                                              {typeid(SimpleAnimation), "SimpleAnimation"}};
