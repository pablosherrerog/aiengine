#pragma once
#include <string>
class Layer
{
   public:
    explicit Layer(const std::string& a_name);
    virtual ~Layer() = default;
    virtual void onUpdate(float a_timeStepSec) = 0;
    virtual void onAttach() = 0;  // init functions etc..
    virtual void onDetach() = 0;
    const std::string& getName() { return m_name; };

   private:
    std::string m_name = "no name defined";
};