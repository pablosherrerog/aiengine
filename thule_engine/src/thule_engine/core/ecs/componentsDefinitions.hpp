// This header is created to make easier importing all components

// Components
#include <thule_engine/components/camera.hpp>
#include <thule_engine/components/directionalLight.hpp>
#include <thule_engine/components/geometry.hpp>
#include <thule_engine/components/material.hpp>
#include <thule_engine/components/metadata.hpp>
#include <thule_engine/components/pointLight.hpp>
#include <thule_engine/components/relationship.hpp>
#include <thule_engine/components/renderable.hpp>
#include <thule_engine/components/simpleAnimation.hpp>
#include <thule_engine/components/spotLight.hpp>
#include <thule_engine/components/transform.hpp>
