#pragma once
#include <bitset>
#include <cstdint>
#define NULL_ENTITY \
    UINT32_MAX  // If we use NULL is going to be 0 which is a valid entity index, so we define our
                // own with the max value possible
// ECS
using Entity = std::uint32_t;
constexpr const Entity MAX_ENTITIES = 5000;
using ComponentType =
    std::uint8_t;  // 8 bits, so it supports 256 a maximum different component type (from 0 to 255)
constexpr const ComponentType MAX_COMPONENTS = 32;
using Signature = std::bitset<MAX_COMPONENTS>;