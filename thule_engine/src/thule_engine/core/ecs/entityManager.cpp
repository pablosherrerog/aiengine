// extracted from https://code.austinmorlan.com/austin/ecs
#include "thule_engine/core/ecs/entityManager.hpp"

#include <iostream>
#include <string>

#include "thule_engine/common/exceptionUtils.hpp"
#include "thule_engine/common/log.hpp"
#include "thule_engine/common/utils.hpp"
#include "thule_engine/core/ecs/typesECS.hpp"

Entity EntityManager::createEntity()
{
    // We have 3 options
    // 1. The limit of entities has been reached, no entity can be added
    // 2. There are some unused entities in between since they were removed
    // 3. There are some unused entities at the enda
    // The reason the entity values are in consecutive positions is in case we use the Entity as ids
    // in some array
    Entity entityToAdd = NULL_ENTITY;

    if (m_numEntitiesActive >= MAX_ENTITIES)
    {
        THULE_ASSERTION_ERROR("Error! max entities " + std::to_string(MAX_ENTITIES) +
                              " limit reached");
    }
    if (!removedEntitiesAvailable.empty())
    {
        entityToAdd = removedEntitiesAvailable.front();
        removedEntitiesAvailable.pop();
    }
    else
    {
        entityToAdd = m_nextSequentialEntityAvailable;
        m_nextSequentialEntityAvailable++;
    }
    m_entities[entityToAdd] = true;  // 1 if the entity exists, 0 if it doesn't.
    m_numEntitiesActive++;
    resetSignature(entityToAdd);
    Log::debug("Added entity " + std::to_string(entityToAdd));
    return entityToAdd;
}
void EntityManager::removeEntity(Entity a_entityToRemove)
{
    if (!m_entities[a_entityToRemove])
    {
        Utils::printErrorAndExit("Error! the entity you are trying to remove was not added before");
    }
    else
    {
        m_entities[a_entityToRemove] = false;
        removedEntitiesAvailable.push(a_entityToRemove);
        Log::debug("Removed entity " + std::to_string(a_entityToRemove));
        m_numEntitiesActive--;
    }
}
bool EntityManager::isEntityInRange(Entity a_entity)
{
    if (a_entity >= MAX_ENTITIES || a_entity < 0)
    {
        Log::error("Error! entity " + std::to_string(a_entity) + " outside range");
        return false;
    }
    return true;
}
int EntityManager::getActiveEntities() { return m_numEntitiesActive; }

void EntityManager::resetSignature(Entity a_entity) { m_Signatures[a_entity].reset(); }
void EntityManager::setSignature(Entity entity, Signature signature)
{
    if (!isEntityInRange(entity))
    {
        return;
    }

    m_Signatures[entity] = signature;
}
Signature EntityManager::getSignature(Entity entity)
{
    if (!isEntityInRange(entity))
    {
        return NULL_ENTITY;
    }

    return m_Signatures[entity];
}

bool EntityManager::entityExists(Entity a_entity)
{
    if (!isEntityInRange(a_entity))
    {
        return false;
    }
    if (!m_entities[a_entity])
    {
        return false;
    }
    return true;
}
