#pragma once
// extracted from https://code.austinmorlan.com/austin/ecs

#include <array>
#include <cassert>
#include <string>
#include <thule_engine/common/utils.hpp>
#include <unordered_map>

#include "thule_engine/common/exceptionUtils.hpp"
#include "thule_engine/core/commonCoreUtils.hpp"
#include "thule_engine/core/ecs/typesECS.hpp"

class IComponentArray
{
   public:
    virtual ~IComponentArray() = default;
    virtual void EntityDestroyed(Entity entity) = 0;
};
template <typename T>
class ComponentArray : public IComponentArray
{
   public:
    void InsertData(Entity entity, const T& component)
    {
        if (mEntityToIndexMap.find(entity) != mEntityToIndexMap.end())
        {
            THULE_ASSERTION_ERROR(std::string("Component ") + typeid(T).name() +
                                  " added to same entity " + std::to_string(entity) +
                                  " more than once.");
        }
        // TODO: maybe it would be interesting to allow two entities sharing one component to avoid
        // data redundancy. E.g if I clone 1 million cubes I don't want to store the geometry
        // component for all of them.

        // Put new entry at end
        size_t newIndex = mSize;
        mEntityToIndexMap[entity] = newIndex;
        mIndexToEntityMap[newIndex] = entity;
        mComponentArray[newIndex] = component;
        ++mSize;
    }

    void RemoveData(Entity entity)
    {
        assert(mEntityToIndexMap.find(entity) != mEntityToIndexMap.end() &&
               "Removing non-existent component.");

        // Copy element at end into deleted element's place to maintain density
        size_t indexOfRemovedEntity = mEntityToIndexMap[entity];
        size_t indexOfLastElement = mSize - 1;
        mComponentArray[indexOfRemovedEntity] = mComponentArray[indexOfLastElement];

        // Update map to point to moved spot
        Entity entityOfLastElement = mIndexToEntityMap[indexOfLastElement];
        mEntityToIndexMap[entityOfLastElement] = indexOfRemovedEntity;
        mIndexToEntityMap[indexOfRemovedEntity] = entityOfLastElement;

        mEntityToIndexMap.erase(entity);
        mIndexToEntityMap.erase(indexOfLastElement);

        --mSize;
    }

    bool hasComponent(Entity a_entity)
    {
        return mEntityToIndexMap.find(a_entity) != mEntityToIndexMap.end();
    }

    T& GetData(Entity entity)
    {
        if (!hasComponent(entity))
        {
            THULE_ASSERTION_ERROR(std::string("Error! Retrieving non-existent ") +
                                  CommonCoreUtils::getComponentName<T>() +
                                  " component for entity " + std::to_string(entity));
        }
        return mComponentArray[mEntityToIndexMap[entity]];
    }

    void EntityDestroyed(Entity entity) override
    {
        if (mEntityToIndexMap.find(entity) != mEntityToIndexMap.end())
        {
            RemoveData(entity);
        }
    }

   private:
    std::array<T, MAX_ENTITIES> mComponentArray{};
    std::unordered_map<Entity, size_t> mEntityToIndexMap{};
    std::unordered_map<size_t, Entity> mIndexToEntityMap{};
    size_t mSize{};
};
