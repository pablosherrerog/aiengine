#include <thule_engine/core/events/appEvents.hpp>

#include "glm/fwd.hpp"
#include "thule_engine/core/input/input.hpp"
#include "thule_engine/geom/geomDef.hpp"

namespace app_events
{
MouseDisplacementWindowEvent::MouseDisplacementWindowEvent(vec2_t a_lastPosition)
{
    m_lastPosition = a_lastPosition;
    m_extraDisplacement = vec2_t(0, 0);
}
vec2_t MouseDisplacementWindowEvent::getCurrentMousePos() { return Input::getMousePosition(); }
/*
In case the mouse has reached a window boundary and is going to be moved
    Args:
        a_boundaryExitPos: boundary position from where the mouse was considered out (without
   offset).
        a_boundaryEntryPos: boundary position from where the mouse was moved (without
   offset).

    E.g: if the x boundaries are a_boundaryExitPos.x = 3 and a_boundaryEntryPos.x=8 and the last
position was 5 then the displacement would be -2 and the new last position would be the
position 8.

*/
void MouseDisplacementWindowEvent::resetLastMousePositionByBoundary(vec2_t a_boundaryExitPos,
                                                                    vec2_t a_boundaryEntryPos)
{
    // First calculate the distance from the last position to the boundary.
    m_extraDisplacement += a_boundaryExitPos - m_lastPosition;
    // Secondly reset the last position to the entry boundary.
    m_lastPosition = a_boundaryEntryPos;
}

// Check how much it moved from the last position plus any extra displacement.
vec2_t MouseDisplacementWindowEvent::getTotalDisplacement()
{
    // Check how much it moved from the last position plus any extra displacement.
    return getMouseDisplacement() + m_extraDisplacement;
}
}  // namespace app_events