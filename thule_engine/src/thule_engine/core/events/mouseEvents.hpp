#include <string>

#include "event.hpp"

namespace MouseEvents
{
class ScrollEvent : public Event
{
   public:
    ScrollEvent(float a_offsetX, float a_offsetY) : m_offsetX(a_offsetX), m_offsetY(a_offsetY) {}
    float getOffsetX() const { return m_offsetX; }
    float getOffsetY() const { return m_offsetY; }
    EVENT_CLASS_TYPE(k_mouseScroll);

    std::string toString() const override
    {
        std::stringstream ss;
        ss << "Scroll event: " << m_offsetX << "(x), " << m_offsetY << "(y)";
        return ss.str();
    }

    event_types::EventType type() const override { return event_types::EventType::k_mouseScroll; }

   private:
    float m_offsetX, m_offsetY;
};
}  // namespace MouseEvents