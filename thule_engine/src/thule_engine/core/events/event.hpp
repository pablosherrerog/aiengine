#pragma once
#include <cstddef>
#include <functional>
#include <map>
#include <memory>
#include <queue>
#include <sstream>
#include <string>
#include <vector>

// To be able to get the type of the event without the need of the instance.
#define EVENT_CLASS_TYPE(type)                                                        \
    static constexpr event_types::EventType eventType = event_types::EventType::type; \
    std::string getName() const override { return #type; }

namespace event_types
{
enum class EventType : int
{
    k_none = 0,
    k_mouseMoved,
    k_windowResize,
    k_sceneViewportResize,
    k_mouseScroll,
    k_keyboard,
    k_activeEditor,
    k_entityDeleted,
    k_entityDeselected,
    k_guiPanelOnFocus,
    k_guiPanelNotOnFocus,
    k_guiPanelMouseWrapped,
    k_framebufferInitialization,
    k_fpsUpdated
};
enum EventCategory
{
    k_none = 0,
    k_keyboardInput
};
};  // namespace event_types

class Event
{
   public:
    virtual event_types::EventType type() const = 0;
    virtual std::string getName() const = 0;
    virtual std::string toString() const { return getName(); }
};

class Dispatcher
{
   public:
    using SlotType = std::function<void(const Event &)>;
    // If you want to assign a function to be called after an event is broadcasted
    static void subscribe(const event_types::EventType &descriptor, Dispatcher::SlotType &&slot);
    // If you want to call all subcribers to that particular event.
    static void broadcast(const Event &event);
    // If you want to stack events to process them later.
    template <typename T>
    static void post(T a_event)
    {
        // TODO(Pablo): Make the queue a circular queue to avoid having the possibility for it
        // to occupy too much space in memory without being consumed.
        // TODO: Maybe std::type_index is enough?
        // https://stackoverflow.com/questions/9859390/use-data-type-class-type-as-key-in-a-map
        event_types::EventType type = a_event.type();
        // TODO: In one of the computers that I tested to run the code I got the following error
        // XTREE Exception thrown: read access violation. Scary->_Myhead was nullptr. Didn't managed
        // to solve it. Pending to solve.
        // The temporal solution is to comment the m_postedEvents parts and to add to the
        // CameraControlSystem::init() function Directly the starting event
        // app_events::WindowsResizeEvent resizeEvent(1280, 720); onWindowsResize(resizeEvent);
        Dispatcher::m_postedEvents<T>[type].push(a_event);
    }
    template <typename T>
    static std::queue<T> getPostedEvent(bool removeEventAfterwards = true)
    {
        event_types::EventType type = T::eventType;
        std::queue<T> queueEvents = Dispatcher::m_postedEvents<T>[type];
        if (removeEventAfterwards)
        {
            Dispatcher::m_postedEvents<T>.erase(type);
        }
        return queueEvents;
    }

   private:
    // Maybe it could be better using queues and a Dispatcher per type
    // https://github.com/Pseudomanifold/Events
    static std::map<event_types::EventType, std::vector<Dispatcher::SlotType>> m_observers;
    template <typename T>
    static std::map<event_types::EventType, std::queue<T>> m_postedEvents;  // declaration
};
template <typename T>
std::map<event_types::EventType, std::queue<T>> Dispatcher::m_postedEvents;  // definition
