#include <memory>
#include <string>
#include <thule_engine/core/ecs/componentsDefinitions.hpp>
#include <thule_engine/core/events/event.hpp>
#include <thule_engine/geom/geomDef.hpp>

#include "thule_engine/core/ecs/typesECS.hpp"

namespace app_events
{
class WindowResizeEvent : public Event
{
   public:
    WindowResizeEvent(unsigned int a_width, unsigned int a_height)
        : m_width(a_width), m_height(a_height)
    {
    }

    unsigned int getWidth() const { return m_width; }
    unsigned int getHeight() const { return m_height; }
    EVENT_CLASS_TYPE(k_windowResize);

    std::string toString() const override
    {
        std::stringstream ss;
        ss << "WindowResizeEvent: " << m_width << ", " << m_height;
        return ss.str();
    }

    event_types::EventType type() const override { return eventType; }

   private:
    unsigned int m_width, m_height;
};
class SceneViewportResize : public Event
{
    // Note: it's assumed for now that there is only one scene viewport to render/resize.
   public:
    SceneViewportResize(unsigned int a_width, unsigned int a_height)
        : m_width(a_width), m_height(a_height)
    {
    }

    unsigned int getWidth() const { return m_width; }
    unsigned int getHeight() const { return m_height; }
    EVENT_CLASS_TYPE(k_sceneViewportResize);

    std::string toString() const override
    {
        std::stringstream ss;
        ss << "SceneViewportResize: " << m_width << ", " << m_height;
        return ss.str();
    }

    event_types::EventType type() const override { return eventType; }

   private:
    unsigned int m_width, m_height;
};
class EditorWindowActive : public Event
{
   public:
    explicit EditorWindowActive(bool a_isActive) : m_isActive(a_isActive){};
    std::string toString() const override
    {
        std::stringstream ss;
        ss << "EditorWindowActive";
        return ss.str();
    }
    bool isActive() const { return m_isActive; }
    EVENT_CLASS_TYPE(k_activeEditor);
    event_types::EventType type() const override { return eventType; }

   private:
    bool m_isActive = false;
};

class DeselectEntity : public Event
{
   public:
    explicit DeselectEntity(Entity a_entity) : m_deselectedEntity(a_entity) {}
    Entity getDeselectedEntity() const { return m_deselectedEntity; }
    EVENT_CLASS_TYPE(k_entityDeselected);
    event_types::EventType type() const override { return eventType; }

   private:
    Entity m_deselectedEntity = NULL_ENTITY;
};

class GuiPanelOnFocus : public Event
{
   public:
    explicit GuiPanelOnFocus(std::string a_panelName) : m_namePanelOnFocus(a_panelName) {}
    std::string getPanelOnFocus() const { return m_namePanelOnFocus; }
    EVENT_CLASS_TYPE(k_guiPanelOnFocus);
    event_types::EventType type() const override { return eventType; }

   private:
    std::string m_namePanelOnFocus;
};
class GuiPanelNotOnFocus : public Event
{
   public:
    EVENT_CLASS_TYPE(k_guiPanelNotOnFocus);
    event_types::EventType type() const override { return eventType; }
};
class MouseDisplacementWindowEvent : public Event
{
   public:
    EVENT_CLASS_TYPE(k_guiPanelMouseWrapped);
    event_types::EventType type() const override { return eventType; }
    explicit MouseDisplacementWindowEvent(vec2_t a_lastPosition);
    static vec2_t getCurrentMousePos();
    void resetLastMousePositionByBoundary(vec2_t a_boundaryExitPos, vec2_t a_boundaryEntryPos);
    // void resetLastPosition() { m_lastPosition = vec2_t(0, 1234); }
    void resetLastPosition() { m_lastPosition = getCurrentMousePos(); }
    void resetDisplacement() { m_extraDisplacement = vec2_t(0, 0); }
    vec2_t getTotalDisplacement();
    vec2_t m_lastPosition;
    vec2_t m_extraDisplacement;

   private:
    vec2_t getMouseDisplacement() const
    {
        // e.g:
        //    If I was at 2 and now I'm at 8 I moved 8 - 2 = 6
        //    If I was at 8 and now I'm at 2 I moved 2 - 8 = -6
        return getCurrentMousePos() - m_lastPosition;
    }
};
}  // namespace app_events