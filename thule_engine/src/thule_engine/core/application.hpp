#pragma once
#include <memory>
#include <string>
#include <thule_engine/core/window.hpp>
#include <vector>

class Layer;
class Application
{
   public:
    Application(const std::string& a_name, const float& a_windowsWidth,
                const float& a_windowsHeight);
    void pushLayer(std::unique_ptr<Layer> a_layer);
    void run();
    // Returns reference to the app. (We are assuming only one application can be running at the
    // same time).
    static Application& getApp() { return *m_appPtr; };
    void close();
    Window& getWindow() const { return *m_windowPtr; }
    static constexpr const float k_fpsUpdateRateInSeconds{
        1.0F};  // How many seconds to send an event to notify about the current FPS.

   private:
    static Application* m_appPtr;
    std::string m_name = "no name";
    std::unique_ptr<Window> m_windowPtr;
    std::vector<std::unique_ptr<Layer>> m_layers;
    float m_lastFrameTimeSec;
    float m_secondsSinceLastFPSUpdate{0};
    bool m_running = false;
};