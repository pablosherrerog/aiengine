// Include the profiler dependencies if the profiler is used, if not just use empty defines.

// For more info check the step 4 of this guide
// https://luxeengine.com/integrating-tracy-profiler-in-cpp/
#ifdef TRACY_ENABLE
#include "Tracy.hpp"
#endif

#ifdef TRACY_ENABLE
#define PROFILER_FRAME_MARK FrameMark
#define PROFILER_SCOPED ZoneScoped
#define PROFILER_SCOPEDN(name) ZoneScopedN(name)
#define PROFILER_LOG(text, size) TracyMessage(text, size)
#else
#define PROFILER_FRAME_MARK
#define PROFILER_SCOPED
#define PROFILER_SCOPEDN(name)
#define PROFILER_LOG(text, size)
#endif
