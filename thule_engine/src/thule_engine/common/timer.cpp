// Timer for benchmarking from The Cherno video https://www.youtube.com/watch?v=YG4jexlSAjc.
#include "thule_engine/common/timer.hpp"

#include <chrono>
#include <iostream>
#include <locale>

Timer::Timer() { m_startTimePoint = std::chrono::high_resolution_clock::now(); }
Timer::~Timer() { getMilliseconds(); }
// Stops the counters and returns the milliseconds ellapsed since the object was created.
double Timer::getMilliseconds() const
{
    auto endTimePoint = std::chrono::high_resolution_clock::now();
    auto start = std::chrono::time_point_cast<std::chrono::microseconds>(m_startTimePoint)
                     .time_since_epoch()
                     .count();
    auto end = std::chrono::time_point_cast<std::chrono::microseconds>(endTimePoint)
                   .time_since_epoch()
                   .count();

    auto duration = end - start;
    double ms = static_cast<double>(duration) * 0.001;
    return ms;
}
void Timer::printInfo() const { printInfo(""); }
void Timer::printInfo(const std::string &a_extraInfo) const
{
    double ms = getMilliseconds();
    std::cout << a_extraInfo << "Time spent " << ms << "ms"
              << " or " << ms * 0.001 << "sec" << std::endl;
}
