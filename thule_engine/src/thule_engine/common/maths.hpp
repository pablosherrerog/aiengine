#pragma once
#include "thule_engine/geom/geomDef.hpp"

class Maths
{
   public:
    static mat4_t transformationMatrixCalculation(const vec3_t& a_trans, const quat_t& a_rotation,
                                                  const vec3_t& a_scale);

    static quat_t eulerDegToQuaternion(const vec3_t& a_eulerAnglesDeg);
    static vec3_t quaternionToEuler(const quat_t& a_quaternion);
};
