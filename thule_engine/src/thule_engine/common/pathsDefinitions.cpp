#include "pathsDefinitions.hpp"

#include <filesystem>
#include <iostream>
#include <string>

#include "cmake_definitions.hpp"
#include "utils.hpp"
namespace fs = std::filesystem;

using namespace common;
// since the vars are static they need before a definition
std::string Definitions::m_mainFolderPath = "Not initialized";
std::string Definitions::m_engine_path = "Not initialized";
std::string Definitions::m_defaultMeshesPath = "Not initialized";
std::string Definitions::m_sourcePath = "Not initialized";
std::string Definitions::m_shadersFolderPath = "Not initialized";
std::string Definitions::m_resourcesFolderPath = "Not initialized";
std::string Definitions::m_texturesFolderPath = "Not initialized";

void Definitions::initializeDefinitions()
{
#ifndef SOURCE_DIR_PATH
    Utils::printErrorAndWaitExit(
        "Error! SOURCE_DIR_PATH should be defined by the cmake configure_file, check if something "
        "went wrong running cmake")
#endif
        m_mainFolderPath = SOURCE_DIR_PATH;
    m_engine_path = ENGINE_SOURCE_DIR_PATH;
    m_sourcePath = m_engine_path + "\\src";
    m_shadersFolderPath = m_sourcePath + "\\thule_engine\\render\\shaders";
    m_resourcesFolderPath = m_mainFolderPath + "\\resources";
    m_texturesFolderPath = m_resourcesFolderPath + "\\default_resources\\textures";
    m_defaultMeshesPath = m_resourcesFolderPath + "\\default_resources\\meshes";
}
