#pragma once
#include "thule_engine/geom/geomDef.hpp"
class PointLight
{
   public:
    // We will use the position from the transform

    color3_t ambient;
    color3_t diffuse;
    color3_t specular;
    ;
    // To calculate attenuation formula
    // F_att = 1.0/constant + linear * d + quadratic * d^2
    // https://learnopengl.com/Lighting/Light-casters
    float linear = 0.07F;
    float quadratic = 0.017F;
    PointLight()
        : ambient{color3_t(0.0, 0.0, 0.0)},
          diffuse{color3_t(0.0, 0.0, 0.0)},
          specular{color3_t(0.0, 0.0, 0.0)} {};
};