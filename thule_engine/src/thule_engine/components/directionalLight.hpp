#pragma once
#include "thule_engine/geom/geomDef.hpp"
class DirectionalLight
{
   public:
    // We will use the direction/rotation from the transform

    color3_t ambient;
    color3_t diffuse;
    color3_t specular;
    DirectionalLight()
        : ambient{color3_t(0.0, 0.0, 0.0)},
          diffuse{color3_t(0.0, 0.0, 0.0)},
          specular{color3_t(0.0, 0.0, 0.0)} {};
};