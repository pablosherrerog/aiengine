#pragma once
#include <iostream>
#include <memory>
#include <vector>

#include "thule_engine/geom/geomDef.hpp"
#include "thule_engine/render/shader.hpp"
#include "thule_engine/render/textures/texture.hpp"

class Material
{
   public:
    std::shared_ptr<Shader> shader{nullptr};
    std::vector<std::shared_ptr<Texture>> textures;  // normal map, diffuse map etc...
    color3_t ambient;
    color3_t diffuse;
    color3_t specular;
    float shininess = 0;

    Material()
        : ambient{color3_t(0.0, 0.0, 0.0)},
          diffuse{color3_t(0.0, 0.0, 0.0)},
          specular{color3_t(0.0, 0.0, 0.0)} {};
    // Custom copy constructor (if we want to copy data to clone it or such)
    Material(const Material& a_material)
    {
        // Create a second shader with the data of the first one.
        // Note: Beware because doing this is beneficial when cloning materials but if you
        // do Material matb = g_coordinator.getComponent<Material>(a_entity); you are creating a
        // copy of the shader class. Instead use  Material &mat =
        // g_coordinator.getComponent<Material>(a_entity); if you want to avoid calling the copy
        // constructor

        shader = std::make_shared<Shader>(*a_material.shader);

        textures = a_material.textures;
        ambient = a_material.ambient;
        diffuse = a_material.diffuse;
        specular = a_material.specular;
        shininess = a_material.shininess;
    }
};