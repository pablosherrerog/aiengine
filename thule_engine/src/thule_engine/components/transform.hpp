#pragma once

//#include "glm/trigonometric.hpp"
#include "thule_engine/common/maths.hpp"
#include "thule_engine/geom/geomDef.hpp"

// In addition to the attributes, the transform component have functions, while I think is not the
// usual way to deal with components in this case I think it simplifies the interaction with the
// transform component. Later a performance review could be done to optimize it better.
struct Transform
{
    Transform()
    {
        m_localPosition = vec3_t(0, 0, 0);
        m_localRotation = quat_t(1, 0, 0, 0);
        m_localScale = vec3_t(1, 1, 1);
        forward = vec3_t(0, 0, -1);
        up = vec3_t(0, 1, 0);
        right = vec3_t(1, 0, 0);
    }
    // TODO: it should be possible to extract the forward, right, up vector from the transformation
    // matrix or with a global rotation quaternion, review it.
    vec3_t forward;
    vec3_t up;
    vec3_t right;
    // We keep a global transformation to avoid recaulculating the matrix if nothing changed.
    mat4_t globalTransf = mat4_t(1.0F);

    void translate(const vec3_t &a_translation)
    {
        setLocalPosition(a_translation + m_localPosition);
    }
    void setLocalPosition(vec3_t a_position)
    {
        m_dirty = true;
        m_localPosition = a_position;
    }
    // rotation in degrees.
    void rotate(const vec3_t &a_eulerRotation)
    {
        setLocalRotation(m_localRotation * Maths::eulerDegToQuaternion(a_eulerRotation));
    }

    void setLocalRotation(quat_t a_rotation)
    {
        m_dirty = true;
        m_localRotation = a_rotation;
    }

    void setLocalRotation(const vec3_t &a_eulerRotation)
    {
        setLocalRotation(Maths::eulerDegToQuaternion(a_eulerRotation));
    }
    void scaling(const vec3_t &a_scale) { setLocalScale(a_scale * m_localScale); }
    void setLocalScale(vec3_t a_scale)
    {
        m_dirty = true;
        m_localScale = a_scale;
    }

    bool isDirty() const { return m_dirty; }
    void setDirty(bool a_dirty) { m_dirty = a_dirty; }
    // Global pos could also be extracted from the transformation matrix
    vec3_t getLocalPosition() { return m_localPosition; }
    quat_t getLocalRotation() { return m_localRotation; }
    vec3_t getLocalRotationEuler() { return glm::degrees(glm::eulerAngles(m_localRotation)); }
    vec3_t getLocalScale() { return m_localScale; }
    vec3_t getGlobalPosition()
    {
        // Global position is updated in the transpormationUpdaterSystem, if this function is
        // called before, it would return the default position (0,0,0)
        return vec3_t(globalTransf[3]);
    }

   private:
    bool m_dirty{true};  // means that the global transformation matrix needs to be recalculated
    vec3_t m_localPosition;
    quat_t m_localRotation;
    vec3_t m_localScale;
};
