#pragma once
#include "thule_engine/geom/geomDef.hpp"
class SimpleAnimation
{
   public:
    vec3_t rotationPerSecond;
    vec3_t transformationStartingPos;
    vec3_t transformationTargetPos;
    vec3_t currentTargetPos;
    float transformationSpeed = 1;
    bool goingToTargetPos = true;  // true for going to target pose, false to go to starting pos
    bool loopTranslationAnimation = true;
    SimpleAnimation()
        : rotationPerSecond{vec3_t(0, 0, 0)},
          transformationStartingPos{vec3_t(0, 0, 0)},
          transformationTargetPos{vec3_t(0, 0, 0)},
          currentTargetPos{vec3_t(0, 0, 0)} {};
};