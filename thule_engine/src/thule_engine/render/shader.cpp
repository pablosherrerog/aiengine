#include "shader.hpp"

#include <glad/glad.h>

#include <iostream>
#include <string>

#include "thule_engine/common/log.hpp"
#include "thule_engine/common/profiler.hpp"
#include "thule_engine/common/utils.hpp"

//#include "texture.h"
Shader::Shader(std::string a_pathVertexShader, std::string a_pathFragmentShader)
{
    loadShaderProgram(a_pathVertexShader, a_pathFragmentShader);
}
void Shader::checkSuccessShaderCompilation(unsigned int a_shader, std::string a_type)
{
    int success;
    char infoLog[512];
    glGetShaderiv(a_shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(a_shader, 512, NULL, infoLog);
        Log::error("Error in " + a_type + " shader, log:\n" + infoLog);
        Utils::printErrorAndWaitExit("ERROR LOADING " + a_type + " SHADER");
    }
    else
    {
        Log::debugLow("Success loading " + a_type + " shader");
    }
}
void Shader::checkSuccessProgramLinking(unsigned int a_program)
{
    int success;
    char infoLog[512];

    glGetProgramiv(a_program, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(a_program, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }
    else
    {
        Log::debugLow("Success linking program");
    }
}

Shader::~Shader()
{
    // delete texture
    for (size_t i = 0; i < texturesPtrs.size(); i++)
    {
        delete (texturesPtrs[i]);
    }
}

unsigned int Shader::loadShaderProgram(std::string a_pathVertexShader,
                                       std::string a_pathFragmentShader)
{
    m_vertexShaderPath = a_pathVertexShader;
    m_fragmentShaderPath = a_pathFragmentShader;
    // vertex shader
    std::string vertexShaderString = Utils::readtTxtFile(a_pathVertexShader);
    const char* vertexShaderCstChr = vertexShaderString.c_str();
    vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexShaderCstChr, NULL);
    glCompileShader(vertexShader);
    checkSuccessShaderCompilation(vertexShader, "vertex");
    // fragment shader
    std::string fragmentShaderString = Utils::readtTxtFile(a_pathFragmentShader);
    const char* fragmentShaderCstChr = fragmentShaderString.c_str();
    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderCstChr, NULL);
    glCompileShader(fragmentShader);
    checkSuccessShaderCompilation(fragmentShader, "fragment");
    // shader program (combines shaders output with input)
    shaderProgram =
        glCreateProgram();  // creates program and returns ID reference to the created object
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);
    checkSuccessProgramLinking(shaderProgram);
    // delete shaders objects since we don't need them
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    return 0;
}

void Shader::useMaterialShader()
{
    glUseProgram(
        shaderProgram);  // all the shader and rendering calls will now the combined shaders
}
void Shader::setBoolUniform(const std::string& name, bool value) const
{
    glUniform1i(getUniformLocation(name), (int)value);
}
void Shader::setIntUniform(const std::string& name, int value) const
{
    glUniform1i(getUniformLocation(name), value);
}
void Shader::setFloatUniform(const std::string& name, float value) const
{
    glUniform1f(getUniformLocation(name), value);
}
void Shader::setMat4(const std::string& name, const glm::mat4& mat) const
{
    glUniformMatrix4fv(getUniformLocation(name), 1, GL_FALSE, &mat[0][0]);
}

void Shader::setVec3(const std::string& name, const glm::vec3& a_vec3) const
{
    glUniform3fv(getUniformLocation(name), 1, &a_vec3[0]);
}
void Shader::setVec4(const std::string& name, const glm::vec4& a_vec4)
{
    glUniform4fv(getUniformLocation(name), 1, &a_vec4[0]);
}

// This funtion is going to allow us to avoid getting the uniform location from the shader if
// we already have it. Searching in the dictionary is much faster that communicating with the GPU
// https://www.youtube.com/watch?v=nBB0LGSIm5Q&list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2&index=28
GLint Shader::getUniformLocation(const std::string& a_uniformName) const
{
    PROFILER_SCOPED;
    if (m_uniformLocationCache.find(a_uniformName) != m_uniformLocationCache.end())
    {
        return m_uniformLocationCache[a_uniformName];
    }
    std::string tmpStr{"First time getting uniform location: " + a_uniformName};
    PROFILER_LOG(tmpStr.c_str(), tmpStr.size());
    GLint location = glGetUniformLocation(shaderProgram, a_uniformName.c_str());

    /*if (location == -1) {
            Utils::printErrorAndExit("Error! uniform " + a_uniformName + " was not found in the
    shader");
    }*/
    m_uniformLocationCache[a_uniformName] = location;
    return location;
}
