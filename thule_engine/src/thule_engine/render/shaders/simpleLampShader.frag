#version 330 core
struct MaterialColors {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
}; 
out vec4 FragColor;
uniform MaterialColors materialColors;

void main()
{
    // FragColor = vec4(1.0); // set all 4 vector values to 1.0
    FragColor = vec4(materialColors.diffuse, 1.0); // set it to the color of the light
}