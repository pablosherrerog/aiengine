#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 normal;
out vec3 fragPos;

void main()
{
    gl_Position = projection * view * model * vec4(aPos, 1.0);
    //normal = aNormal;
    // TODO: the inverse should not be used since is very costly a normal matrix should be used instead
    normal = mat3(transpose(inverse(model))) * aNormal;  

    // frag pos used for lighting should be in world space without camera calculations
    fragPos = vec3(model * vec4(aPos, 1.0));
} 