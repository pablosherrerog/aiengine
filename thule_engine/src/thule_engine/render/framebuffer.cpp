#include <memory>
#include <thule_engine/render/framebuffer.hpp>
#include <thule_engine/render/openglFramebuffer.hpp>

std::shared_ptr<Framebuffer> Framebuffer::create(const FramebufferSpecification& a_specification)
{
    return std::make_shared<OpenGLFramebuffer>(a_specification);
}