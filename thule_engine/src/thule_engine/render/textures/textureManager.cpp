#include <string>
#include <thule_engine/common/exceptionUtils.hpp>
#include <thule_engine/render/textures/openglTexture.hpp>
#include <thule_engine/render/textures/texture.hpp>
#include <thule_engine/render/textures/textureManager.hpp>

std::shared_ptr<Texture> TextureManager::create(const std::string& a_path,
                                                TextureType a_textureType)
{
    // Here we could do some ifdef to change it to Vulkan instead of OpenGL
    // Also Consider return if it failed the load operation instead of throwing an exception
    return std::make_shared<OpenGLTexture2D>(a_path, a_textureType);
}

std::string TextureManager::textureTypeToStr(TextureType a_textureType)
{
    switch (a_textureType)
    {
        case TextureType::none:
            return "no_texture";
            break;
        case TextureType::diffuse:
            return "texture_diffuse";
            break;
        case TextureType::specular:
            return "texture_specular";
            break;
        case TextureType::normal:
            return "texture_normal";
            break;
        case TextureType::height:
            return "texture_height";
            break;
        case TextureType::emissive:
            return "texture_emissive";
            break;
        default:
            THULE_EXCEPTION(
                "Error! Texture type to str not supported, should be added to the switch "
                "statement");
    }
}