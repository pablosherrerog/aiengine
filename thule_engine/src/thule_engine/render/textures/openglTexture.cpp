#include "thule_engine/render/textures/openglTexture.hpp"

#include <glad/glad.h>

#include <cstdint>
#include <filesystem>
#include <memory>
#include <string>
#include <thule_engine/common/exceptionUtils.hpp>
#include <thule_engine/common/log.hpp>
#include <thule_engine/common/pathsDefinitions.hpp>
#include <thule_engine/common/utils.hpp>

// Sean Barret load texture
//#ifndef STB_IMAGE_IMPLEMENTATION
//#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
//#endif
#include <stb_image.h>

#include <glm/gtx/string_cast.hpp>

OpenGLTexture2D::OpenGLTexture2D(const std::filesystem::path& a_path, TextureType a_textureType)
{
    m_path = a_path;
    m_textureType = a_textureType;
    // how many textures you need to generate (1) and the ID
    glGenTextures(1, &m_textureID);

    // Check relative path.
    if (!std::filesystem::exists(m_path))
    {
        std::filesystem::path fullPath = common::Definitions::m_texturesFolderPath;
        fullPath /= m_path;
        fullPath = fullPath.string();
        // If even with the relative path doesn't work give error.
        if (!std::filesystem::exists(fullPath))
        {
            THULE_EXCEPTION("Error! the texture path " + fullPath.string() + " doesn't exists" +
                            stbi_failure_reason());
        }
        m_path = fullPath;
    }
    int width = 0;
    int height = 0;
    int nrComponents;

    m_width = width;
    m_height = height;
    unsigned char* data = stbi_load(m_path.string().c_str(), &width, &height, &nrComponents, 0);
    if (data)
    {
        GLenum format;
        if (nrComponents == 1)
        {
            format = GL_RED;
        }
        else if (nrComponents == 3)
        {
            format = GL_RGB;
        }
        else if (nrComponents == 4)
        {
            format = GL_RGBA;
        }
        else
        {
            THULE_EXCEPTION("Error! texture has " + std::to_string(nrComponents) +
                            " texture components which is not supported");
        }
        // when we bind it so all next texture commands affect the bound texture
        glBindTexture(GL_TEXTURE_2D, m_textureID);
        // configure the wrapping/filtering options
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        // load the texture data
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        // generate mipmaps (smaller version of the texture in case the camera is far away)
        glGenerateMipmap(GL_TEXTURE_2D);

        glBindTexture(GL_TEXTURE_2D, 0);  // unbind texture
        stbi_image_free(data);
    }

    else
    {
        THULE_EXCEPTION("Texture failed to load at path: " + m_path.string());
        stbi_image_free(data);
    }
}

OpenGLTexture2D::~OpenGLTexture2D()
{
    // Note: Careful when removing a texure (e.g. if you create a local copy and goes out of scope
    // all textures with the id will be freed).
    glDeleteTextures(1, &m_textureID);
}

void OpenGLTexture2D::bind(std::uint32_t a_textureUnit) const
{
    // Active proper texture unit before binding
    glActiveTexture(GL_TEXTURE0 + a_textureUnit);
    glBindTexture(GL_TEXTURE_2D, m_textureID);
    // An alternative for opengl version 4.5 seems to be glBindTextureUnit
    // https://stackoverflow.com/questions/60513272/alternative-for-glbindtextureunit-for-opengl-versions-less-than-4-5
}
