#pragma once
#include <cstdint>
#include <filesystem>
#include <string>

#include "texture.hpp"

class OpenGLTexture2D : public Texture
{
   public:
    OpenGLTexture2D() = default;
    explicit OpenGLTexture2D(const std::filesystem::path& a_path, TextureType a_textureType);
    ~OpenGLTexture2D() override;

    void bind(unsigned int a_textureUnit) const override;
    std::uint32_t getWidth() const override { return m_width; }
    std::uint32_t getHeight() const override { return m_height; }
    std::uint32_t getTextureID() const override { return m_textureID; }
    TextureType getTextureType() const override { return m_textureType; }
    std::filesystem::path getPath() const override { return m_path; }
    void setTextureType(TextureType a_textureType) override { m_textureType = a_textureType; };
    void setTextureID(std::uint32_t a_textureID) override { m_textureID = a_textureID; };
    void setTexturePath(std::filesystem::path a_texturePath) override { m_path = a_texturePath; };

   private:
    // Useful for debug, could be moved to an asset manager and map it with the texture object.
    std::filesystem::path m_path = "Not defined";
    std::uint32_t m_width = 123456;
    std::uint32_t m_height = 123456;
    std::uint32_t m_textureID = 123456;
    TextureType m_textureType = TextureType::none;
};