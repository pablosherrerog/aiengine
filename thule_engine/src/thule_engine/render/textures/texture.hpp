#pragma once
#include <cstdint>
#include <filesystem>
#include <memory>
#include <string>

enum class TextureType
{
    none,
    diffuse,
    specular,
    normal,
    height,
    emissive
};
class Texture
{
   public:
    // std::string type;  // normal map, diffuse map etc...
    virtual ~Texture() = default;
    virtual void setTextureType(TextureType a_textureType) = 0;
    virtual void setTextureID(std::uint32_t a_textureID) = 0;
    virtual void setTexturePath(std::filesystem::path a_texturePath) = 0;
    virtual TextureType getTextureType() const = 0;
    virtual std::uint32_t getTextureID() const = 0;
    virtual std::uint32_t getWidth() const = 0;
    virtual std::uint32_t getHeight() const = 0;
    virtual std::filesystem::path getPath() const = 0;
    virtual void bind(unsigned int a_textureUnit) const = 0;
};
