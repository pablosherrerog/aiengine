#pragma once
#include <stdint.h>

#include <memory>
#include <thule_engine/render/framebuffer.hpp>

class OpenGLFramebuffer : public Framebuffer
{
   public:
    explicit OpenGLFramebuffer(const FramebufferSpecification& a_specification);
    ~OpenGLFramebuffer() override;
    // It means the specification in no longer valid so recalculations should be done.
    void updateFromSpecification() override;
    void resize(uint32_t a_width, uint32_t a_height) override;
    const FramebufferSpecification& getSpecification() const override { return m_specification; };
    uint32_t getRendererID() const override { return m_rendererID; }
    uint32_t getTextureColorID() const override { return m_textureColorID; }

   private:
    uint32_t m_rendererID = 12345;
    uint32_t m_textureColorID = 12345;
    uint32_t m_rbo = 12345;

    FramebufferSpecification m_specification;
};