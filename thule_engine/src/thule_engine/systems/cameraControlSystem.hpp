#pragma once
#include "thule_engine/core/ecs/system.hpp"
#include "thule_engine/geom/geomDef.hpp"
struct GLFWwindow;
struct Transform;
class Camera;
class Event;

class CameraControlSystem : public System
{
   public:
    void init();
    void update(float a_dt);

   private:
    float orbitRadians = 0;
    float constrainAngle(float a_x);
    void calculateViewMatrix(Transform *a_trans, Camera *a_camera);
    void processInput(Transform *a_trans, Camera *a_camera, float a_dt);
    void orbitAroundPoint(Transform *a_trans, Camera *a_camera, vec3_t a_center, float a_dt);
    void resetCameraRotation(Entity a_cameraEnt);
    void resetCameraRotation(Camera *a_camera);
    void recalculateCameraProjection(Camera *a_camera);
    void onSceneViewportResize(const Event &a_event);
    void onScroll(const Event &a_event);
};