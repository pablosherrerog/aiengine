#pragma once
#include <thule_engine/core/ecs/system.hpp>
class TransformationUpdaterSystem : public System
{
   public:
    void update();

   private:
    void updateGlobalTransf(Entity a_entity);
    void updateGlobalTransf(Entity a_entity, Entity a_parentWithTransf);
};