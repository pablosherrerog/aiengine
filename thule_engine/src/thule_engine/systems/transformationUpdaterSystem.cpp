// Note: This system is a bit peculiar because it's not using only the system entities.
// It needs to use entities that could not have the Transform component)
// That's the downside of not having for all entities a Transform component by default.

#include "transformationUpdaterSystem.hpp"

#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/string_cast.hpp>
#include <set>

#include "thule_engine/common/maths.hpp"
#include "thule_engine/common/profiler.hpp"
#include "thule_engine/common/utils.hpp"
#include "thule_engine/components/metadata.hpp"
#include "thule_engine/components/relationship.hpp"
#include "thule_engine/components/transform.hpp"
#include "thule_engine/core/ecs/coordinator.hpp"
#include "thule_engine/core/ecs/typesECS.hpp"
#include "thule_engine/geom/geomDef.hpp"

extern Coordinator g_coordinator;
// This function should run in hierarchy order (from parent to children).
// a_parentWithTransf: is there because there can be entities without the transform component in
// between the parent with the tranform and the next children with a transform component.
void TransformationUpdaterSystem::updateGlobalTransf(Entity a_entity, Entity a_parentWithTransf)
{
    if (a_entity == NULL_ENTITY)
    {
        return;
    }
    // auto relationship = g_coordinator.getComponent<Relationship>(a_entity);
    std::vector<Entity> children = g_coordinator.getChildren(a_entity);
    Entity parentWithTransf = a_parentWithTransf;
    // If the entity has a Transform component it will appear in the mEntitites set
    bool hasTransformEntity = mEntities.find(a_entity) != mEntities.end();
    if (hasTransformEntity)
    {
        Transform parentTransf;
        auto& transf = g_coordinator.getComponent<Transform>(a_entity);
        if (parentWithTransf != NULL_ENTITY)
        {
            parentTransf = g_coordinator.getComponent<Transform>(parentWithTransf);
            // If the parent was dirty the children must be recalculate the global transformation
            // using the updated parent transformation
            if (parentTransf.isDirty())
            {
                transf.setDirty(true);
            }
        }
        // If the transformation is dirty the global transformation must be recalculated
        if (transf.isDirty())
        {
            transf.globalTransf =
                parentTransf.globalTransf *
                Maths::transformationMatrixCalculation(
                    transf.getLocalPosition(), transf.getLocalRotation(), transf.getLocalScale());
        }
        parentWithTransf = a_entity;
    }
    // Update the children
    for (Entity& child : children)
    {
        updateGlobalTransf(child, parentWithTransf);
    }
    // The transform component will be updated, mark it as not dirty.
    if (hasTransformEntity)
    {
        auto& transf = g_coordinator.getComponent<Transform>(a_entity);
        transf.setDirty(false);
    }
}

void TransformationUpdaterSystem::updateGlobalTransf(Entity a_entity)
{
    updateGlobalTransf(a_entity, NULL_ENTITY);
}

void TransformationUpdaterSystem::update()
{
    PROFILER_SCOPED;
    // 1. Get a list of the root entities
    // 2. Go through all entities from parent to child to update the global transformation.
    for (Entity rootEntity : g_coordinator.getRootEntities())
    {
        updateGlobalTransf(rootEntity);
    }
}