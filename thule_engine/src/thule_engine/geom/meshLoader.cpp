#include "thule_engine/geom/meshLoader.hpp"

#include <memory.h>

#include <iostream>
#include <string>
#include <thule_engine/render/textures/texture.hpp>
#include <vector>

#include "thule_engine/common/log.hpp"
#include "thule_engine/common/pathsDefinitions.hpp"
#include "thule_engine/common/utils.hpp"
#include "thule_engine/components/geometry.hpp"
#include "thule_engine/components/material.hpp"
#include "thule_engine/components/metadata.hpp"
#include "thule_engine/components/relationship.hpp"
#include "thule_engine/components/renderable.hpp"
#include "thule_engine/components/transform.hpp"
#include "thule_engine/core/ecs/coordinator.hpp"
#include "thule_engine/core/ecs/typesECS.hpp"
#include "thule_engine/geom/assimp_loader/assimpDef.hpp"
#include "thule_engine/geom/assimp_loader/assimpLoader.hpp"
#include "thule_engine/geom/geomDef.hpp"

extern Coordinator g_coordinator;

Entity MeshLoader::loadMesh(std::string a_path)
{
    Log::debug("Loading mesh from path " + a_path);
    std::shared_ptr<assimp::Model> modelPtr = AssimpLoader::loadGeom(a_path);
    Entity parentNode = loadAssimpNode(modelPtr, NULL_ENTITY);  // root entity has no parents
    if (parentNode == NULL_ENTITY)
    {
        Utils::printErrorAndExit("Error! No entity loaded from path " + a_path);
    }
    return parentNode;
}
Entity MeshLoader::loadAssimpNode(const std::shared_ptr<assimp::Model>& a_modelPtr, Entity a_parent)
{
    Entity entity = g_coordinator.createEntity(a_parent, a_modelPtr->name);
    // We assume that all loaded entities has a transform component
    g_coordinator.addComponent<Transform>(entity);
    if (a_modelPtr->meshPtr != nullptr)
    {
        // Add geometry info
        assimp::Mesh assimpMesh = *a_modelPtr->meshPtr;
        Geometry geomComponent;
        for (auto& vertice : assimpMesh.vertices)
        {
            Vertex vertex;
            vertex.pos = vertice.position;
            vertex.uv = vertice.texCoords;
            vertex.normal = vertice.normal;
            geomComponent.vertices.emplace_back(vertex);
        }
        if (assimpMesh.indices.size() % 3 != 0)
        {
            Utils::printErrorAndExit("Error! assimp mesh faces are not defined in triangles");
        }
        for (int fi = 0; fi < assimpMesh.indices.size(); fi += 3)
        {
            geomComponent.faces.emplace_back(face_t(assimpMesh.indices[fi],
                                                    assimpMesh.indices[fi + int(1)],
                                                    assimpMesh.indices[fi + int(2)]));
        }
        g_coordinator.addComponent<Geometry>(entity, geomComponent);
        // Transform assimp texture into our type of texture and add it to a material
        // TODO: evaluate how to implement more materials here (e.g if you have the diffuse texture
        // you don't need the diffuse plain color component?)
        // Add material info
        Material mat = Material();
        mat.shader = std::make_shared<Shader>(
            common::Definitions::m_shadersFolderPath + "/texturedLightShader.vert",
            common::Definitions::m_shadersFolderPath + "/texturedLightShader.frag");
        mat.diffuse =
            vec3_t(a_modelPtr->meshPtr->assimpColor[0], a_modelPtr->meshPtr->assimpColor[1],
                   a_modelPtr->meshPtr->assimpColor[2]);
        mat.ambient =
            vec3_t(a_modelPtr->meshPtr->assimpAmbient[0], a_modelPtr->meshPtr->assimpAmbient[1],
                   a_modelPtr->meshPtr->assimpAmbient[2]);
        mat.specular =
            vec3_t(a_modelPtr->meshPtr->assimpSpecular[0], a_modelPtr->meshPtr->assimpSpecular[1],
                   a_modelPtr->meshPtr->assimpSpecular[2]);
        mat.shininess = 32.0F;
        for (const std::shared_ptr<Texture>& texturePtr : assimpMesh.textures)
        {
            mat.textures.emplace_back(texturePtr);
        }

        Renderable ren = Renderable();
        g_coordinator.addComponent<Material>(entity, mat);
        g_coordinator.addComponent<Renderable>(entity, ren);
    }

    // Load the children nodes
    for (std::shared_ptr<assimp::Model>& assimpChild : a_modelPtr->children)
    {
        loadAssimpNode(assimpChild, entity);
    }

    Log::debug("Loaded entity " + std::to_string(entity) + " " +
               g_coordinator.getComponent<Metadata>(entity).name + " from loader");
    return entity;
}
