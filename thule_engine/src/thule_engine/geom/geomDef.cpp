#include "thule_engine/geom/geomDef.hpp"

ColoredVertex::ColoredVertex(vec3_t a_pos, color3_t a_color, normal_t a_normal, uv_t a_uv)
    : pos(a_pos), color(a_color), normal(a_normal), uv(a_uv)
{
}
Vertex::Vertex(vec3_t a_pos, normal_t a_normal, uv_t a_uv) : pos(a_pos), normal(a_normal), uv(a_uv)
{
}
