#include "thule_engine/geom/geomResources.hpp"

#include <vector>

#include "thule_engine/geom/geomDef.hpp"

Square::Square()
{
    std::vector<float> stdVert = std::vector<float>{
        0.5f,  0.5f,  0.0f,  // top right
        0.5f,  -0.5f, 0.0f,  // bottom right
        -0.5f, -0.5f, 0.0f,  // bottom left
        -0.5f, 0.5f,  0.0f   // top left
    };
    std::vector<int> stdInd = std::vector<int>{
        0, 1, 3,  // first Triangle
        1, 2, 3   // second Triangle
    };
    normal_t defaultNormal = normal_t(0.0f, 0.5f, 0.0f);
    uv_t defaultUV = uv_t(0.0f, 0.0f);

    Vertex v1 = Vertex(vec3_t(stdVert[0], stdVert[1], stdVert[2]), defaultNormal, uv_t(1.0f, 1.0f));

    Vertex v2 = Vertex(vec3_t(stdVert[3], stdVert[4], stdVert[5]), defaultNormal, uv_t(1.0f, 0.0f));

    Vertex v3 = Vertex(vec3_t(stdVert[6], stdVert[7], stdVert[8]), defaultNormal, uv_t(0.0f, 0.0f));

    Vertex v4 =
        Vertex(vec3_t(stdVert[9], stdVert[10], stdVert[11]), defaultNormal, uv_t(0.0f, 1.0f));

    vertices.emplace_back(v1);
    vertices.emplace_back(v2);
    vertices.emplace_back(v3);
    vertices.emplace_back(v4);

    indices.emplace_back(face_t(stdInd[0], stdInd[1], stdInd[2]));
    indices.emplace_back(face_t(stdInd[3], stdInd[4], stdInd[5]));
}
