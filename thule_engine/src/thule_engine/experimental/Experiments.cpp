#include <experimental/experiments.h>
struct Vector3
{
    float x, y, z;
    Vector3(float a_x, float a_y, float a_z) : x(a_x), y(a_y), z(a_z) {}
};
Experiment::Experiment()
{
    int value = 5;
    int array[5];
    array[0] = 0;
    array[1] = 1;
    array[2] = 2;
    array[3] = 3;
    array[4] = 4;

    Vector3 vector(10, 11, 12);
    int* hvalue = new int;
    *hvalue = 5;
    int* harray = new int[5];
    array[0] = 0;
    array[1] = 1;
    array[2] = 2;
    array[3] = 3;
    array[4] = 4;

    Vector3* hvector = new Vector3(13, 14, 15);
}