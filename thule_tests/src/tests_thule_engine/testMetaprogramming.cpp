#include <gtest/gtest.h>

#include <iostream>
#include <string>
#include <thule_engine/common/log.hpp>
#include <thule_engine/core/metaprograming.hpp>
#include <typeinfo>

TEST(TestIterationThroughTypes, BasicAssertions)
{
    struct Component1
    {
        int a = 1;
    };
    {
        using type0 = typename mp::NthType<0, float, int, std::string, Component1>::type;
        using type2 = typename mp::NthType<2, float, int, std::string, Component1>::type;
        using type3 = typename mp::NthType<3, float, int, std::string, Component1>::type;
        EXPECT_EQ(std::string(typeid(float).name()), std::string(typeid(type0).name()));
        EXPECT_EQ(std::string(typeid(std::string).name()), std::string(typeid(type2).name()));
        EXPECT_EQ(std::string(typeid(Component1).name()), std::string(typeid(type3).name()));
    }

    // Test using typelist, the partial specialization should allow to handle a typelist as input.
    {
        using ComponentList = mp::TypeList<float, int, std::string, Component1>;
        using type0 = typename mp::NthTypeTypeList<0, ComponentList>::type;
        using type2 = typename mp::NthTypeTypeList<2, ComponentList>::type;
        using type3 = typename mp::NthTypeTypeList<3, ComponentList>::type;
        EXPECT_EQ(std::string(typeid(float).name()), std::string(typeid(type0).name()));
        EXPECT_EQ(std::string(typeid(std::string).name()), std::string(typeid(type2).name()));
        EXPECT_EQ(std::string(typeid(Component1).name()), std::string(typeid(type3).name()));
    }
}
