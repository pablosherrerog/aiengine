#pragma once
// If you add/modify a component you need to set here its GUI representation.
#include <imgui/imgui.h>
#include <vcruntime.h>

#include <cstddef>
#include <memory>
#include <string>
#include <thule_engine/common/exceptionUtils.hpp>
#include <thule_engine/core/commonCoreUtils.hpp>
#include <thule_engine/core/ecs/coordinator.hpp>
#include <thule_engine/render/textures/textureManager.hpp>

#include "imgui/imgui_internal.h"
#include "thule_app/gui/editor/sceneHierarchyPanel.hpp"
#include "thule_engine/common/pathsDefinitions.hpp"

// Components
#include "thule_engine/components/directionalLight.hpp"
#include "thule_engine/components/material.hpp"
#include "thule_engine/components/renderable.hpp"
#include "thule_engine/components/simpleAnimation.hpp"
#include "thule_engine/components/spotLight.hpp"
#include "thule_engine/core/ecs/componentsDefinitions.hpp"
// Extracted from SceneHierarchyPanel.cpp Hazel https://github.com/TheCherno/Hazel
extern Coordinator g_coordinator;

// Returns true if it some of the values was modified.
static bool drawVec3Control(const std::string& label, glm::vec3& values, float resetValue = 0.0f,
                            float columnWidth = 100.0f)
{
    bool modified = false;
    ImGuiIO& io = ImGui::GetIO();
    auto* boldFont = io.Fonts->Fonts[0];

    ImGui::PushID(label.c_str());

    ImGui::Columns(2);
    ImGui::SetColumnWidth(0, columnWidth);
    ImGui::Text("%s", label.c_str());
    ImGui::NextColumn();

    ImGui::PushMultiItemsWidths(3, ImGui::CalcItemWidth());
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2{0, 0});

    float lineHeight = GImGui->Font->FontSize + GImGui->Style.FramePadding.y * 2.0f;
    ImVec2 buttonSize = {lineHeight + 3.0f, lineHeight};

    ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{0.8f, 0.1f, 0.15f, 1.0f});
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{0.9f, 0.2f, 0.2f, 1.0f});
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{0.8f, 0.1f, 0.15f, 1.0f});
    ImGui::PushFont(boldFont);
    if (ImGui::Button("X", buttonSize))
    {
        values.x = resetValue;
        modified = true;
    }
    ImGui::PopFont();
    ImGui::PopStyleColor(3);

    ImGui::SameLine();
    if (ImGui::DragFloat("##X", &values.x, 0.1f, 0.0f, 0.0f, "%.2f"))
    {
        modified = true;
    }
    ImGui::PopItemWidth();
    ImGui::SameLine();

    ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{0.2f, 0.7f, 0.2f, 1.0f});
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{0.3f, 0.8f, 0.3f, 1.0f});
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{0.2f, 0.7f, 0.2f, 1.0f});
    ImGui::PushFont(boldFont);
    if (ImGui::Button("Y", buttonSize))
    {
        values.y = resetValue;
        modified = true;
    }
    ImGui::PopFont();
    ImGui::PopStyleColor(3);

    ImGui::SameLine();
    if (ImGui::DragFloat("##Y", &values.y, 0.1f, 0.0f, 0.0f, "%.2f"))
    {
        modified = true;
    }
    ImGui::PopItemWidth();
    ImGui::SameLine();

    ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{0.1f, 0.25f, 0.8f, 1.0f});
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{0.2f, 0.35f, 0.9f, 1.0f});
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{0.1f, 0.25f, 0.8f, 1.0f});
    ImGui::PushFont(boldFont);
    if (ImGui::Button("Z", buttonSize))
    {
        values.z = resetValue;
        modified = true;
    }
    ImGui::PopFont();
    ImGui::PopStyleColor(3);

    ImGui::SameLine();
    if (ImGui::DragFloat("##Z", &values.z, 0.1f, 0.0f, 0.0f, "%.2f"))
    {
        modified = true;
    }
    ImGui::PopItemWidth();

    ImGui::PopStyleVar();

    ImGui::Columns(1);

    ImGui::PopID();
    return modified;
}

template <typename T>
class GUIComponent
{
   public:
    static void drawGUIComponent(Entity a_entity){
        // If the other GUIComponents have full specialization (e.g template <> class
        // GUIComponent<Transform> it will call their function there instead of this one)

        // Not giving error, if the component gui is not defined then it will not draw anything.
        // QUESTION: Not sure why I can't add the components name to the static assert name when
        // it's known at compile time.
        // std::string componentName = CommonCoreUtils::getComponentName<T>();
        /*static_assert(
            false, "Error! the component has not a explicit definition, define it in this
           header.");*/
    };
};
template <>
class GUIComponent<Transform>
{
   public:
    static void drawGUIComponent(Entity a_entity)
    {
        auto& component = g_coordinator.getComponent<Transform>(a_entity);
        vec3_t translation = component.getLocalPosition();
        vec3_t rotation = component.getLocalRotationEuler();
        vec3_t scale = component.getLocalScale();
        if (drawVec3Control("Translation", translation))
        {
            component.setLocalPosition(translation);
        }
        if (drawVec3Control("Rotation", rotation))
        {
            component.setLocalRotation(rotation);
        }
        if (drawVec3Control("Scale", scale, 1.0F))
        {
            component.setLocalScale(scale);
        }
    };
};
template <>
class GUIComponent<Renderable>
{
   public:
    static void drawGUIComponent(Entity a_entity)
    {
        auto& component = g_coordinator.getComponent<Renderable>(a_entity);
        std::string componentName = CommonCoreUtils::getComponentName<Renderable>();
        std::string strId = (std::to_string(a_entity) +
                             CommonCoreUtils::getComponentName<Renderable>() + "debug_info");
        // We set the id with the component name and entity to avoid collisions with other trees.
        if (ImGui::TreeNodeEx(strId.c_str(), 0, "debug info"))
        {
            ImGui::Text("VBO id: %s", std::to_string(component.VBO).c_str());
            ImGui::Text("VAO id: %s", std::to_string(component.VAO).c_str());
            ImGui::Text("EBO id: %s", std::to_string(component.EBO).c_str());
            ImGui::TreePop();
        }
    }
};

template <>
class GUIComponent<Material>
{
   public:
    static void drawGUIComponent(Entity a_entity)
    {
        auto& component = g_coordinator.getComponent<Material>(a_entity);
        std::string strId = (std::to_string(a_entity) +
                             CommonCoreUtils::getComponentName<Renderable>() + "debug_info");
        // Question: while in imgui demo they use a c cast for the pointer type conversion in
        // the cherno use a glm::value_ptr not sure about the difference.
        ImGui::ColorEdit3("ambient", reinterpret_cast<float*>(&component.ambient));
        ImGui::ColorEdit3("diffuse", reinterpret_cast<float*>(&component.diffuse));

        ImGui::ColorEdit3("specular", reinterpret_cast<float*>(&component.specular));
        if (ImGui::TreeNodeEx((strId + "textures").c_str(), ImGuiTreeNodeFlags_DefaultOpen,
                              "Textures info"))
        {
            for (const std::shared_ptr<Texture>& texturePtr : component.textures)
            {
                std::uint32_t textureID = texturePtr->getTextureID();
                ImGui::Image((void*)textureID, ImVec2{64.0F, 64.0F});
                std::string textureTypeStr =
                    TextureManager::textureTypeToStr(texturePtr->getTextureType());

                ImGui::TextWrapped(
                    "%s", (textureTypeStr + " path: " + texturePtr->getPath().string()).c_str());
                if (ImGui::TreeNodeEx((strId + textureTypeStr).c_str(), 0, "%s",
                                      (textureTypeStr + " debug info").c_str()))
                {
                    ImGui::LabelText("texture id", "%s",
                                     std::to_string(texturePtr->getTextureID()).c_str());
                    ImGui::TreePop();
                }
            }
            ImGui::TreePop();
        }
        if (ImGui::TreeNodeEx(strId.c_str(), 0, "debug info"))
        {
            if (component.shader == nullptr)
            {
                ImGui::TextWrapped("vertex shader path: empty path did you reset the material?");
                ImGui::TextWrapped("fragment shader path: empty path did you reset the material?");
            }
            else
            {
                ImGui::TextWrapped(
                    "%s",
                    ("vertex shader path: " + component.shader->getVertexShaderPath()).c_str());
                ImGui::TextWrapped(
                    "%s",
                    ("fragment shader path: " + component.shader->getFragmentShaderPath()).c_str());
            }
            for (auto& texture : component.textures)
            {
                ImGui::LabelText(
                    "texture ", "id: %i, type: %s", texture->getTextureID(),
                    TextureManager::textureTypeToStr(texture->getTextureType()).c_str());
            }
            ImGui::TreePop();
        }
    }
};

template <>
class GUIComponent<DirectionalLight>
{
   public:
    static void drawGUIComponent(Entity a_entity)
    {
        auto& component = g_coordinator.getComponent<DirectionalLight>(a_entity);
        // Question: while in imgui demo they use a c cast for the pointer type conversion in
        // the cherno use a glm::value_ptr not sure about the difference.
        ImGui::ColorEdit3("ambient", reinterpret_cast<float*>(&component.ambient));
        ImGui::ColorEdit3("diffuse", reinterpret_cast<float*>(&component.diffuse));
        ImGui::ColorEdit3("specular", reinterpret_cast<float*>(&component.specular));
    }
};
template <>
class GUIComponent<PointLight>
{
   public:
    static void drawGUIComponent(Entity a_entity)
    {
        auto& component = g_coordinator.getComponent<PointLight>(a_entity);
        // ColorEdit3 expects a range between 0 and 1
        ImGui::ColorEdit3("ambient", reinterpret_cast<float*>(&component.ambient));
        ImGui::ColorEdit3("diffuse", reinterpret_cast<float*>(&component.diffuse));
        ImGui::ColorEdit3("specular", reinterpret_cast<float*>(&component.specular));
        ImGui::DragFloat("linear", &component.linear, 0.01F, 0.0F, 3.0F, "%.2f");
        ImGui::DragFloat("quadratic", &component.quadratic, 0.01F, 0.0F, 3.0F, "%.2f");
    }
};

template <>
class GUIComponent<SpotLight>
{
   public:
    static void drawGUIComponent(Entity a_entity)
    {
        auto& component = g_coordinator.getComponent<SpotLight>(a_entity);
        // ColorEdit3 expects a range between 0 and 1
        ImGui::ColorEdit3("ambient", reinterpret_cast<float*>(&component.ambient));
        ImGui::ColorEdit3("diffuse", reinterpret_cast<float*>(&component.diffuse));
        ImGui::ColorEdit3("specular", reinterpret_cast<float*>(&component.specular));
        ImGui::DragFloat("linear", &component.linear, 0.01F, 0.0F, 3.0F, "%.2f");
        ImGui::DragFloat("quadratic", &component.quadratic, 0.01F, 0.0F, 3.0F, "%.2f");
    }
};
template <>
class GUIComponent<Camera>
{
   public:
    static void drawGUIComponent(Entity a_entity)
    {
        auto& component = g_coordinator.getComponent<Camera>(a_entity);
        ImGui::SliderFloat("Field of view", &component.fov, 1.0e-5, 125.0F, "%.2f");
        // Doubt: not sure if codify this clamp in a system or do a function to do so.
        if (ImGui::SliderFloat("near", &component.nearDistance, 1.0e-5, 125.0F, "%.2f"))
        {
            if (component.nearDistance >= component.farDistance)
            {
                component.nearDistance = component.farDistance - 0.01F;
            }
        }
        if (ImGui::SliderFloat("far", &component.farDistance, 1.0e-5, 125.0F, "%.2f"))
        {
            if (component.nearDistance >= component.farDistance)
            {
                component.farDistance = component.nearDistance + 0.01F;
            }
        }
        ImGui::SliderFloat("Rotation sensivity", &component.rotationSensitivity, 0.01F, 0.8F,
                           "%.2f");
    };
};
template <>
class GUIComponent<SimpleAnimation>
{
   public:
    static void drawGUIComponent(Entity a_entity)
    {
        auto& component = g_coordinator.getComponent<SimpleAnimation>(a_entity);
        drawVec3Control("rotation per second", component.rotationPerSecond);
        drawVec3Control("transformation starting pos", component.transformationStartingPos);
        drawVec3Control("transformation target pos", component.transformationTargetPos);
        ImGui::DragFloat("speed transformation", &component.transformationSpeed, 0.1F, 0.0F, 10.0F,
                         "%.2f");
        ImGui::Checkbox("loop translation", &component.loopTranslationAnimation);
    }
};