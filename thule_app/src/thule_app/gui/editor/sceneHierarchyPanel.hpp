#pragma once
#include <imgui/imgui.h>

#include <thule_engine/core/ecs/coordinator.hpp>
#include <thule_engine/core/metaprograming.hpp>

#include "thule_app/gui/guiPanel.hpp"
#include "thule_engine/components/relationship.hpp"
#include "thule_engine/core/commonCoreUtils.hpp"
#include "thule_engine/core/ecs/typesECS.hpp"

extern Coordinator g_coordinator;

class SceneHierarchyPanel : public GuiPanel
{
   public:
    SceneHierarchyPanel() { m_panelName = getPanelName(); };
    void onUpdate() override;
    void onAttach() override;
    void deselectEntity(Entity a_entity);
    static constexpr const char* getPanelName() { return "Scene Hierarchy"; };

   private:
    static void drawComponents(Entity a_entity);
    void onImguiRender();
    void processInput() override;
    Entity m_selectedEntity = NULL_ENTITY;
    template <typename T>
    static void addComponentLogic(Entity a_entity)
    {
        std::string componentName = CommonCoreUtils::getComponentName<T>();
        if (ImGui::MenuItem(componentName.c_str()))
        {
            if (!g_coordinator.hasComponent<T>(a_entity))
            {
                g_coordinator.addComponent<T>(a_entity, T{});
            }
            else
            {
                std::string entityName = g_coordinator.getComponent<Metadata>(a_entity).name;
                Log::warning("Couldn't add " + componentName + " since this entity " + entityName +
                             " already has the component!");
            }
        }
    }

    // Call it with addComponentsLogic<0>
    template <int N>
    static void addComponentsLogic(Entity a_entity)
    {
        using ComponentType = typename mp::NthTypeTypeList<N, ComponentList>::type;
        // Both Metadata and Relationship are special cases
        if (!mp::IsSame<ComponentType, Metadata>::value &&
            !mp::IsSame<ComponentType, Relationship>::value)
        {
            addComponentLogic<ComponentType>(a_entity);
        }
        if constexpr (N + 1 < ComponentList::size())
        {
            addComponentsLogic<N + 1>(a_entity);
        }
    }
};