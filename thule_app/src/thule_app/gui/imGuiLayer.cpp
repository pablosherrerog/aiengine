// Part of this code was extracted from https://github.com/TheCherno/Hazel
// and https://github.com/ocornut/imgui/blob/docking/examples/example_glfw_opengl3/main.cpp
// https://www.youtube.com/watch?v=lZuje-3iyVE&list=PLlrATfBNZ98dC-V-N3m0Go4deliWHPFwT&index=24&t=510s
#include "thule_app/gui/imGuiLayer.hpp"

#include <GLFW/glfw3.h>
#include <glad/glad.h>
#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

#include <memory>
#include <string>

#include "thule_app/gui/editor/sceneHierarchyPanel.hpp"
#include "thule_app/gui/editor/sceneViewport.hpp"
#include "thule_app/gui/editor/statsPanel.hpp"
#include "thule_app/gui/guiPanel.hpp"
#include "thule_app/gui/imGuiUtils.hpp"
#include "thule_app/sandboxLayer.hpp"
#include "thule_engine/common/exceptionUtils.hpp"
#include "thule_engine/common/log.hpp"
#include "thule_engine/common/profiler.hpp"
#include "thule_engine/components/camera.hpp"
#include "thule_engine/core/application.hpp"
#include "thule_engine/core/ecs/typesECS.hpp"
#include "thule_engine/core/events/appEvents.hpp"
#include "thule_engine/core/events/engineEvents.hpp"
#include "thule_engine/core/events/keyboardEvents.hpp"
#include "thule_engine/core/input/keyCodes.hpp"
#include "thule_engine/geom/geomDef.hpp"
#include "thule_engine/render/framebuffer.hpp"
#include "thule_engine/render/textures/texture.hpp"
#include "thule_engine/render/textures/textureManager.hpp"

ImGuiLayer::ImGuiLayer() : Layer("ImGuiLayer") {}
ImVec4 g_clearColor = ImVec4(0.45F, 0.55F, 0.60F, 1.00F);
bool g_showAnotherWindow = true;
extern Coordinator g_coordinator;
void ImGuiLayer::onAttach()
{
    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
    // io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;    // Enable Docking
    io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;  // Enable Multi-Viewport / Platform Windows
    // io.ConfigFlags |= ImGuiConfigFlags_ViewportsNoTaskBarIcons;
    // io.ConfigFlags |= ImGuiConfigFlags_ViewportsNoMerge;

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    // When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look
    // identical to regular ones.
    ImGuiStyle& style = ImGui::GetStyle();
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        style.WindowRounding = 0.0F;
        style.Colors[ImGuiCol_WindowBg].w = 1.0F;
    }

    ImGuiUtils::setDarkThemeColors();  // Custom colors from the Cherno

    Application& app = Application::getApp();
    auto* window = static_cast<GLFWwindow*>(app.getWindow().getNativeWindow());

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    // Doubt: not sure if this version should match the other that I have (in application.cpp)
    ImGui_ImplOpenGL3_Init("#version 330");
    // Create the gui panels (windows) inside the main editor window.
    m_guiPanels[SceneHierarchyPanel::getPanelName()] = std::make_unique<SceneHierarchyPanel>();
    m_guiPanels[SceneViewport::getPanelName()] = std::make_unique<SceneViewport>();
    m_guiPanels[StatsPanel::getPanelName()] = std::make_unique<StatsPanel>();
    for (const auto& [key, guiPanel] : m_guiPanels)
    {
        guiPanel->onAttach();
    }
    // If you are dragging the mouse in one panel deactivate the input in other panels
    // e.g. we don't want to hover into the hierarchy panel entities when rotating in the viewport.
    auto panelOnFocusLambda = [&](const Event& a_event)
    {
        auto guiPanelOnFocusEvent = static_cast<const app_events::GuiPanelOnFocus&>(a_event);
        std::string panelOnFocus = guiPanelOnFocusEvent.getPanelOnFocus();
        for (const auto& [key, guiPanel] : m_guiPanels)
        {
            // Disable all inputs except the panel on focus.
            if (key != panelOnFocus)
            {
                Log::debug("Disabling gui panel " + key);
                guiPanel->disableInput();
            }
        }
    };

    Dispatcher::subscribe(event_types::EventType::k_guiPanelOnFocus, panelOnFocusLambda);

    auto panelNotOnFocusLambda = [&](const Event& a_event)
    {
        auto guiPanelNotOnFocusEvent = static_cast<const app_events::GuiPanelNotOnFocus&>(a_event);
        // Enable the input for all panels
        Log::debug("Enabling all gui panels");
        for (const auto& [key, guiPanel] : m_guiPanels)
        {
            guiPanel->enableInput();
        }
    };
    Dispatcher::subscribe(event_types::EventType::k_guiPanelNotOnFocus, panelNotOnFocusLambda);
}

void ImGuiLayer::onDetach()
{
    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
    // glfwDestroyWindow(window);
    // glfwTerminate();
}

void ImGuiLayer::onUpdate(float a_dt)
{
    PROFILER_SCOPED;
    begin();
    onImguiRender();
    end(a_dt);
}
void ImGuiLayer::begin()
{
    // Start the imgui frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}
void ImGuiLayer::end(float a_dt)
{
    ImGuiIO& io = ImGui::GetIO();
    Window* window = &Application::getApp().getWindow();
    io.DisplaySize = ImVec2(window->getWidth(), window->getHeight());
    // DOUBT: it's needed the delta time? it works without setting it.
    io.DeltaTime = a_dt;

    // Rendering
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        GLFWwindow* backupCurrentContext = glfwGetCurrentContext();
        ImGui::UpdatePlatformWindows();
        ImGui::RenderPlatformWindowsDefault();
        glfwMakeContextCurrent(backupCurrentContext);
    }
}

// In this function we are allow to do any imgui rendering. Will be placed between the functions
// begin and end.
void ImGuiLayer::onImguiRender()
{
    PROFILER_SCOPED;
    // https://www.youtube.com/watch?v=UljMVrQ_zYY&list=PLlrATfBNZ98dC-V-N3m0Go4deliWHPFwT&index=70
    // copied from ShowExampleAppDockSpace() imgui_demo.cpp
    // If we want to use docking space, the viewports allowed to be docked will be the ones
    // inside the dockspace Begin and End.
    static bool dockspaceOpen = true;
    static bool optFullscreen = true;
    static bool optPadding = false;
    static bool optionLaunchDebugImguiWindow = false;
    static ImGuiDockNodeFlags dockspaceFlags = ImGuiDockNodeFlags_None;

    // We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable
    // into, because it would be confusing to have two docking targets within each others.
    ImGuiWindowFlags windowFlags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
    if (optionLaunchDebugImguiWindow)
    {
        // Imgui demo window for testing purposes.
        // Located before defining the next window viewport to avoid instantiating the window in
        // the main viewport.
        ImGui::ShowDemoWindow();
    }
    if (optFullscreen)
    {
        const ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->WorkPos);
        ImGui::SetNextWindowSize(viewport->WorkSize);
        ImGui::SetNextWindowViewport(viewport->ID);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0F);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0F);
        windowFlags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse |
                       ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
        windowFlags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
    }
    else
    {
        dockspaceFlags &= ~ImGuiDockNodeFlags_PassthruCentralNode;
    }

    // When using ImGuiDockNodeFlags_PassthruCentralNode, DockSpace() will render our background
    // and handle the pass-thru hole, so we ask Begin() to not render a background.
    if (dockspaceFlags & ImGuiDockNodeFlags_PassthruCentralNode)
        windowFlags |= ImGuiWindowFlags_NoBackground;

    // Important: note that we proceed even if Begin() returns false (aka window is collapsed).
    // This is because we want to keep our DockSpace() active. If a DockSpace() is inactive,
    // all active windows docked into it will lose their parent and become undocked.
    // We cannot preserve the docking relationship between an active window and an inactive
    // docking, otherwise any change of dockspace/settings would lead to windows being stuck in
    // limbo and never being visible.
    if (!optPadding) ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0F, 0.0F));
    ImGui::Begin("DockSpace Engine", &dockspaceOpen, windowFlags);
    if (!optPadding) ImGui::PopStyleVar();

    if (optFullscreen) ImGui::PopStyleVar(2);

    // Submit the DockSpace
    ImGuiIO& io = ImGui::GetIO();
    if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable)
    {
        ImGuiID dockspaceId = ImGui::GetID("MyDockSpace");
        ImGui::DockSpace(dockspaceId, ImVec2(0.0F, 0.0F), dockspaceFlags);
    }

    if (ImGui::BeginMenuBar())
    {
        if (ImGui::BeginMenu("File"))
        {
            if (ImGui::MenuItem("New", "Ctrl+N"))
            {
                Log::warning("New not implemented yet");
            }
            if (ImGui::MenuItem("Save", "Ctrl+S"))
            {
                Log::warning("Save not implemented yet");
            }
            ImGui::Separator();
            if (ImGui::MenuItem("Exit", "Ctrl+Esc"))
            {
                Application::getApp().close();
            }
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Window"))
        {
            // Disabling fullscreen would allow the window to be moved to the front of other
            // windows, which we can't undo at the moment without finer window depth/z control.
            ImGui::MenuItem("Fullscreen", nullptr, &optFullscreen);
            ImGui::MenuItem("Padding", nullptr, &optPadding);
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Debug"))
        {
            // Usefull in we want to review the imgui functionality.
            ImGui::MenuItem("Launch imgui demo window", nullptr, &optionLaunchDebugImguiWindow);
            ImGui::EndMenu();
        }
        ImGui::EndMenuBar();
    }
    for (const auto& [key, guiPanel] : m_guiPanels)
    {
        guiPanel->onUpdate();
    }
    ImGui::End();
}
