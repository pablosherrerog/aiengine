#include "thule_app/gui/guiPanel.hpp"

#include <imgui/imgui.h>

#include "thule_app/gui/imGuiUtils.hpp"
#include "thule_engine/common/log.hpp"
#include "thule_engine/core/events/appEvents.hpp"
#include "thule_engine/core/events/engineEvents.hpp"

void GuiPanel::wrapMouseIntoPanelWhenPressed()
{
    // Check if the mouse was pressed in the viewport and retain the variable to true until the
    // mouse is released (to retain the value even if the cursor has left the window).
    m_viewportHovered = ImGui::IsWindowHovered();

    if ((ImGui::IsMouseDown(0) || ImGui::IsMouseDown(1)) && m_viewportHovered && m_inputEnabled &&
        !m_viewportMouseDragging)
    {
        std::string panelName = getPanelName();
        Dispatcher::broadcast(app_events::GuiPanelOnFocus(panelName));
        m_viewportMouseDragging = true;
    }
    else if (!(ImGui::IsMouseDown(0) || ImGui::IsMouseDown(1)) && m_viewportMouseDragging)
    {
        m_viewportMouseDragging = false;
        std::string panelName = getPanelName();
        Dispatcher::broadcast(app_events::GuiPanelNotOnFocus());
    }
    if (m_viewportMouseDragging)
    {
        // Make the mouse remain in the viewport area.
        ImGuiUtils::wrapMouseAroundContentRegionFromMouseInput();
    }
}