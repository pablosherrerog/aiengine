cmake_minimum_required(VERSION 3.2)

project(TopProject VERSION 0.0.1)
# Activate profiler tracy if needed
option(TRACY_ENABLE "" OFF)
# option(TRACY_ON_DEMAND "" ON)
if(TRACY_ENABLE)
  add_subdirectory(sharedExternal/tracy)
  # target: TracyClient or alias Tracy::TracyClient
endif()

# We need the compile_commands.json to use clang-tidy
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

add_subdirectory(thule_engine)
if(NOT AVOID_OPENGL)
  # Since thule_app use imgui and imgui uses opengl.
  add_subdirectory(thule_app)
endif()
add_subdirectory(thule_tests)
